<?php

/**
 * Test functionality of the system, checks
 * all basic functionality (adding, removing, listing and searching).
 */
class FirstCest {
  protected $familyName, $givenName;

  public function _before(AcceptanceTester $I) {
    $this->familyName = md5(date('l jS \of F Y h:i:s A'));  // Create random 32 character string
    $this->givenName = md5(date('l jS h:i:s A \of F Y '));  // Create random 32 character string
  }

  // tests
  public function firstPageTest(AcceptanceTester $I) {
    $I->amOnPage('/');
    $I->see('Legg til kontakt');
    $I->see('Vis alle kontakter');
    $I->see('Søk etter kontakter');
  }

  /**
   * Checks that we are able to add and remove Contacts
   *
   * @param AcceptanceTester $I [description]
   */
  public function addContactTest(AcceptanceTester $I) {
    $this->addContact($I);
    $I->see('Kontakten ble registrert');
    $id = $I->grabAttributeFrom('h1', 'data-id');
    $I->amOnPage('deleteContact.php?id='.$id);
    $I->see('Kontakten ble slettet');
  }

  /**
   * Checks that we can see a contact in the contact list

   * @param  AcceptanceTester $I [description]
   */
  public function listContactsTest(AcceptanceTester $I) {
    $this->addContact($I);
    $I->see('Kontakten ble registrert');
    $id = $I->grabAttributeFrom('h1', 'data-id');

    $I->amOnPage('listContacts.php');
    $I->see('Alle kontakter i registeret');
    $I->see($this->givenName);

    $I->amOnPage('deleteContact.php?id='.$id);
    $I->see('Kontakten ble slettet');
  }

  /**
   * Checks that we will find a given contact using the search feature.
   *
   * @param  AcceptanceTester $I [description]
   */
  public function searchContactTest(AcceptanceTester $I) {
    $this->addContact($I);
    $I->see('Kontakten ble registrert');
    $id = $I->grabAttributeFrom('h1', 'data-id');

    $I->amOnPage('searchContact.php?search='.$this->givenName);
    $I->see('Søk etter');
    $I->see($this->familyName);

    $I->amOnPage('deleteContact.php?id='.$id);
    $I->see('Kontakten ble slettet');
  }

  private function addContact(AcceptanceTester $I) {
    $I->amOnPage('addContact.php');
    $I->see('Legg til en kontakt');
    $I->fillField('givenName', $this->givenName);
    $I->fillField('familyName', $this->familyName);
    $I->fillField('email', 'test@test.tt');
    $I->fillField('phone', '12345678');
    $I->click('addContact');
  }
}
