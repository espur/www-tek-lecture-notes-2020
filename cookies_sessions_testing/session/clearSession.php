<?php

session_start();    // Starts a session

session_destroy();  // Removes session, does not affect $_SESSION
?>
<pre><?php
    print_r($_SESSION);
   ?>
</pre>

<?php
  session_unset();  // Should remove $_SESSION, does not work
 ?>

 <pre><?php
     print_r($_SESSION);
    ?>

<?php
  $_SESSION = array();  // Removes content of $_SESSION
 ?>

 <pre><?php
     print_r($_SESSION);
    ?>
 </pre>
<a href="index.php">Tilbake til forsiden</a>
