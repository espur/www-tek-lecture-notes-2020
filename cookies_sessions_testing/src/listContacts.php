<?php

require_once '../../vendor/autoload.php';
require_once 'classes/Contacts.php';
require_once 'classes/DB.php';

$loader = new \Twig\Loader\FilesystemLoader('./twig_templates');
$twig = new \Twig\Environment($loader, [
    /* 'cache' => './compilation_cache', // Only enable cache when everything works correctly */
]);

$contacts = new Contacts(DB::getDBConnection());
$res = $contacts->listContacts ();

echo $twig->render('listContacts.html', $res);
