<?php

require_once '../../vendor/autoload.php';
require_once "classes/Contacts.php";
require_once "classes/DB.php";

$loader = new \Twig\Loader\FilesystemLoader('./twig_templates');
$twig = new \Twig\Environment($loader, [
    /* 'cache' => './compilation_cache', // Only enable cache when everything works correctly */
]);

if (isset($_GET['id'])) {
  $db = DB::getDBConnection();
  /*
  if ($db==null) {
    // show error page and exit
  } */

  $contacts = new Contacts($db);
  $res = $contacts->deleteContact ($_GET['id']);
  $res['data'] = $res;

  echo $twig->render('contactDeleted.html', $res);
}
