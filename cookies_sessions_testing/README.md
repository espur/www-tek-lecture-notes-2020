## startup

docker-compose up -d

## Check contents of /var/www

docker exec -it cookies_sessions_testing_www_1 bash

## Running tests

docker exec -it cookies_sessions_testing_test_1 bash

// codecept bootstrap initialize testing framework

codecept run unit
codecept run acceptance
codecept run
