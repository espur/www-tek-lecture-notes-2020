<?php
/**
 * This is a simple class that shows constructors, destructors
 * and the toString method.
 *
 * @author Øivind Kolloen, roughly a translation of code from https://code.tutsplus.com/tutorials/object-oriented-php-for-beginners--net-12762
 *
 */
class MyClass {
  /**
   * A private variable, can only be manipulated using methods.
   * @var string stores a simple string
   */
  private $prop1 = 'Jeg er en egenskap i klassen';
  /**
   *  A public static variable, no need to instantiating an object to acccess this variable.
   */
  public static $count = 0;

  /**
   * The constructor is called when a new object of this class is constructed.
   */
  public function __construct() {
    echo 'Et objekt av  klassen "', __CLASS__, '" er opprettet!<br />';
    $this->prop1 .= ' "'.__CLASS__.'"';
  }

  /**
   * The desctructor is called when the object is removed/destroyed.
   */
  public function __destruct() {
    echo 'Objektet av klassen "', __CLASS__, '" er fjernet.<br />';
  }

  /**
   * The toString method is called when this object is converted into a string.
   */
  public function __toString() {
    echo 'Bruker toString metoden: ';
    return $this->getProperty();
  }

  /**
   * Method used to set the value of the single property of this object.
   *
   * @param string the new value of the property.
   */
  public function setProperty($newval) {
    $this->prop1 = $newval;
  }

  /**
   * Method used to get the value of the single property of this object.
   *
   * @return string the value of the property.
   */
  protected function getProperty() {
    return $this->prop1 . '<br />';
  }

  /**
   * Static methods can be called without instantiating an object of the class.
   * Calling this method will increase the count by 1.
   */
  public static function plusOne() {
    return 'Vi har nå kommet til tallet ' . ++self::$count . '.<br />';
  }
}

/**
 * This class inherits from MyClass.
 * All properties and methods available in MyClass is also available in this class.
 */
class MyOtherClass extends MyClass {
  /**
   * Called when creating new objects of this class.
   */
  public function __construct() {
    parent::__construct();  // Call the constructor of MyClass (does not happen automatically)
    echo 'Et objekt av klassen ' . __CLASS__ . ' er nå opprettet.<br />';
  }

  /**
   * A new method that does not exist in MyClass.
   */
  public function newMethod() {
    echo 'Denne metoden finnes i klassen ' . __CLASS__ . '.<br />';
  }

  /**
   * Since the property in MyClass i private and the accessor method is
   * protected this is the only way to access the value of the property.
   *
   * @return string the value of the property of MyClass.
   */
  public function callProtected() {
    return $this->getProperty();
  }
}

echo 'Ingen objekter av MyClass er opprettet enda, verdien av count er : '
      .MyClass::$count.'.<br/>';  // Note the placement of the $ sign!

$obj = new MyClass();
//echo "The value of the property is {$obj->getProperty()}";

$obj = new MyOtherClass();
$obj->setProperty('Ny verdi for egenskapen');
echo "The value of the property is {$obj->callProtected()}";


do {
  // Call plusOne without instantiating MyClass
  echo MyClass::plusOne();
} while ( MyClass::$count < 10 );

?>
