## startup

docker-compose up -d

## Check contents of /var/www

docker exec -it lab_uke4_www_1 bash

## Running tests

docker exec -it lab_uke4_test_1 bash

// codecept bootstrap initialize testing framework

codecept run unit
codecept run acceptance
codecept run
