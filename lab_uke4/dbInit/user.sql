CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `givenName` varchar(64) DEFAULT "",
  `familyName` varchar(64) DEFAULT "",
  `department` varchar(128) DEFAULT "",
  `userType` varchar(32) DEFAULT "",
  PRIMARY KEY (`id`)
) ENGINE = InnoDB CHARSET = utf8 COLLATE utf8_bin;