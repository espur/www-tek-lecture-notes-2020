<?php
require_once '../../vendor/autoload.php';
require_once '../classes/DB.php';

$loader = new \Twig\Loader\FilesystemLoader('./twig');
$twig = new \Twig\Environment($loader, [
    /* 'cache' => './compilation_cache', // Only enable cache when everything works correctly */
]);

echo $twig->render('upload.html', array());
