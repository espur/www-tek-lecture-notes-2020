<?php
require_once '../classes/DB.php';

$db = DB::getDBConnection();

$sql = "SELECT name, mime, size, content FROM filesInDB WHERE id=?";
$sth = $db->prepare ($sql);
$sth->execute(array($_GET['id']));

if ($row=$sth->fetch(PDO::FETCH_ASSOC)) {
  header('Content-type: '.$row['mime']);
  header('Content-Disposition: attachment; filename='.$row['name']);
  header('Content-Length: ' . $row['size']);

  echo $row['content'];
} else {
  header("HTTP/1.0 404 Not Found");
}
