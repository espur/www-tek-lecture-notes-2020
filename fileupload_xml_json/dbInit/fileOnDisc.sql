CREATE TABLE `filesOnDisc` (
    `id` bigint(20) NOT NULL AUTO_INCREMENT,
    `owner` bigint(20) NOT NULL,
    `name` varchar(128) COLLATE utf8_bin NOT NULL,
    `mime` varchar(128) COLLATE utf8_bin NOT NULL,
    `size` int(11) NOT NULL,
    `description` text COLLATE utf8_bin NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_bin;