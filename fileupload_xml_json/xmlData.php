<pre>
<?php
$xmlDoc = new DOMDocument();
$xmlDoc->load("http://www.yr.no/sted/Norge/Oppland/Gjøvik/Gjøvik/varsel.xml");

$xpath = new DOMXPath($xmlDoc);
$elements = $xpath->query("/weatherdata/location/name");
if (!is_null($elements)) {
  foreach ($elements as $element) {
    echo $element->nodeValue. "\n";
  }
}

$elements = $xpath->query("/weatherdata/location/location");
if (!is_null($elements)) {
  foreach ($elements as $element) {
    echo $element->getAttribute('altitude')."\n";
  }
}

$elements = $xpath->query("/weatherdata/links/link[2]");
if (!is_null($elements)) {
  foreach ($elements as $element) {
    echo $element->getAttribute('url')."\n\n\n";
  }
}

print_r ($xmlDoc);
