<?php
session_start();

$test = 2;
require_once "classes/User.php";
require_once "classes/DB.php";
require_once '../vendor/autoload.php';

$loader = new \Twig\Loader\FilesystemLoader('./templates');
$twig = new \Twig\Environment($loader, [
    /* 'cache' => './compilation_cache', // Only enable cache when everything works correctly */
]);

$db = DB::getDBConnection();

if ($db==null) {
  echo $twig->render('error.html', array('msg' => 'Unable to connect to the database!'));
  die();  // Abort further execution of the script
}

$user = new User($db);

if ($user->loggedIn()) {
  echo $twig->render('index.html', array('loggedin'=>'yes')); // Add other data as needed
} else {
  echo $twig->render('index.html', array());  // Add data as needed
}
