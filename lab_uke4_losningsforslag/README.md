## startup

docker-compose up -d

## Check contents of /var/www

docker exec -it lab_uke4_losningsforslag_www_1 bash

## Running tests

docker exec -it lab_uke4_losningsforslag_test_1 bash

codecept run unit
codecept run acceptance
codecept run
