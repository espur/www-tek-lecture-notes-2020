CREATE TABLE `testdb`.`contact` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `givenName` VARCHAR(64) NOT NULL,
    `familyName` VARCHAR(64) NOT NULL,
    `email` VARCHAR(128) NOT NULL DEFAULT '',
    `phone` VARCHAR(32) NOT NULL DEFAULT '',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB CHARSET = utf8 COLLATE utf8_bin;