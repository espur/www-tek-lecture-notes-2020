<?php

require_once '../../vendor/autoload.php';
require_once "classes/Contacts.php";

$loader = new \Twig\Loader\FilesystemLoader('./twig_templates');
$twig = new \Twig\Environment($loader, [
    /* 'cache' => './compilation_cache', // Only enable cache when everything works correctly */
]);

if (!isset($_POST['addContact'])) {
  echo $twig->render('addContactForm.html', array());
} else {
  $data['givenName'] = $_POST['givenName'];
  $data['familyName'] = $_POST['familyName'];
  $data['phone'] = $_POST['phone'];
  $data['email'] = $_POST['email'];

  $contacts = new Contacts();
  $res = $contacts->addContact ($data);
  $res['data'] = $data;

  echo $twig->render('contactAdded.html', $res);
}
