<?php

require_once '../../vendor/autoload.php';
require_once "classes/Contacts.php";

$loader = new \Twig\Loader\FilesystemLoader('./twig_templates');
$twig = new \Twig\Environment($loader, [
    /* 'cache' => './compilation_cache', // Only enable cache when everything works correctly */
]);

$data = [];
if  (isset($_GET['id'])) {
  $contacts = new Contacts();
  $data = $contacts->getContact ($_GET['id']);
  echo $twig->render('editContact.html', $data);
} else if (isset($_POST['updateContact'])) {
  $contacts = new Contacts();
  $data = $contacts->updateContact($_POST);
  echo $twig->render('contactUpdated.html', $data);
} else {
  echo $twig->render('error.html', array ("message"=>"Ingen kontaktinformasjon anngitt."));
}
