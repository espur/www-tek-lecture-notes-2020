<?php
require_once "Pinterest.php";

require_once '../vendor/autoload.php';

$loader = new \Twig\Loader\FilesystemLoader('./templates');
$twig = new \Twig\Environment($loader, [
    /* 'cache' => './compilation_cache', // Only enable cache when everything works correctly */
]);

$res = Pinterest::getPinsWithURLS("mathematical riddles fun");
$data['pins'] = $res;

echo $twig->render('index.html', $data);
 ?>
